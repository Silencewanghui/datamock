const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const startAllMock = require('../routes/mockApi/index').startAllMock;

const TagSchema = new Schema({
    // 项目id，唯一标识
    projectId: {
        type: String,
        unique: true,
        require: true
    },
    // 项目标志
    identifier: {
        type: String,
        unique: true,
        require: true
    },
    // 标签列表，key为版本号，value为对应的接口配置数据
    tagList: {
        type: Array,
        require: true
    },
    // 项目当前的版本
    curTag: {
        type: String,
        require: true
    }
});

TagSchema.post('save', function(doc) {
    if (this.isModified('tagList') || this.isNew) {
        // 重新部署全部的Mock服务
        startAllMock()
    }
});

module.exports = mongoose.model('Tag', TagSchema);