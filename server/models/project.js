const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProjectSchema = new Schema({
    // 项目id，唯一标识
    projectId: {
        type: String,
        unique: true,
        require: true
    },
    // 项目标识
    identifier: {
        type: String,
        unique: true,
        require: true
    },
    // 当前版本号
    version: {
        type: String
    }
});

module.exports = mongoose.model('Project', ProjectSchema);