/**
 * 网络请求错误处理
 * 
 * @param {Object} resObj 
 * @param {String} message 
 * @param {String} errDetail 
 * @returns 
 */
function requestErrorHandler(resObj, message, errDetail) {
    return resObj.json({
        status: -1,
        statusInfo: {
            message: message,
            detail: errDetail
        }
    })
}

/**
 * 请求参数验证
 * @param {Object} reqObj 
 * @param {String} param 
 * @param {Array} paramChildProps 
 * @returns 
 */
function paramValidate(reqObj, param, paramChildProps) {
    if (!reqObj[param]) {
        return `缺少参数${param}`
    }
    if (paramChildProps && paramChildProps.length) {
        for (let i = 0, childProp; childProp = paramChildProps[i++];) {
            if (!reqObj[param][childProp]) {
                return `缺少参数${param}.${childProp}`
            }
        }
    }
    return null
}

module.exports = {
    requestErrorHandler: requestErrorHandler,
    paramValidate: paramValidate
}