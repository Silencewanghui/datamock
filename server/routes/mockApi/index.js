const express = require('express');
const Tag = require('../../models/tag');
const Project = require('../../models/project');
const Util = require('../api/util');
const errorHandler = require('../errorHandler');
const RequestParaValidate = require('../api/requestValidate').requestParaValidate
const _cloneDeep = require('lodash/cloneDeep');
const routerList = {}; // 键为projectID，值为对应的router实例

function genDynamicRoute({ identifier, moduleList, version }) {
    routerList[`${identifier}_${version}`] = express.Router()

    // 获取路由生成所需要的数据
    const routeList = Util.getRouteRules({
        identifier: identifier,
        moduleList: moduleList,
        version: version
    });

    routeList.forEach(({
        url,
        requestType,
        requestParaRules,
        data
    }) => {
        const _requestType = requestType.toLowerCase()

        // 注册路由
        routerList[`${identifier}_${version}`][_requestType](url, (req, res) => {
            let requestPara = {};
            if (_requestType === 'get') {
                requestPara = req.query;
            } else if (_requestType === 'post' ||
                _requestType === 'put' || _requestType === 'delete') {
                requestPara = req.body;
            }

            // 验证请求参数
            const errors = RequestParaValidate(requestPara, requestParaRules);

            if (errors.length) {
                return errorHandler.requestErrorHandler(res, errors, '')
            }

            return res.json({
                data: data,
                status: 0
            })
        })
    })
}

function registerMockRoute(app) {
    Tag.find({}, (err, result) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, '数据库查询失败', err)
        }
        while (result.length) {
            const tagItem = _cloneDeep(result.shift())
            const identifier = tagItem.identifier
            const tagList = _cloneDeep(tagItem.tagList)
            tagList.forEach(versionItem => {
                const version = versionItem.tag
                routerList[`${identifier}_${version}`] = undefined
            })
        }
    })

    app.use(function(req, res, next) {
        const identifier = req.path.split('/')[1]
        const version = req.path.split('/')[2]

        routerList[`${identifier}_${version}`](req, res, next)
    })
}

// 启动所有项目所有版本的mock服务
function startAllMock() {
    Tag.find({}, (err, result) => {
        if (err) return err
        while (result.length) {
            const tagItem = _cloneDeep(result.shift())
            const projectId = tagItem.projectId
            const identifier = tagItem.identifier
            const tagList = _cloneDeep(tagItem.tagList)
            tagList.forEach(versionItem => {
                const version = versionItem.tag
                const moduleList = versionItem.data
                genDynamicRoute({
                    identifier,
                    moduleList,
                    version
                })
            })
        }
    })
}

module.exports = {
    registerMockRoute: registerMockRoute,
    genDynamicRoute: genDynamicRoute,
    startAllMock: startAllMock
}