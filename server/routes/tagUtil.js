const Tag = require('../models/tag');
const _cloneDeep = require('lodash/cloneDeep');

/**
 * 获取指定项目id和版本号的接口配置数据
 * @param {String} projectId 
 * @param {String} version 
 * @param {Function} callback 
 */
function getData(projectId, version, callback) {
    Tag.findOne({ projectId: projectId }, (err, result) => {
        if (err) {
            return callback('查询失败', null)
        }
        if (!result) {
            return callback('不存在该项目', null)
        }
        let data = null
        let tagList = _cloneDeep(result.tagList)

        const compareResult = tagList.some(item => {
            if (item.tag === version) {
                data = _cloneDeep(item)
                return true
            }
            return false
        })

        if (!compareResult) {
            return callback('不存在此版本', null)
        }

        return callback(null, data)
    })
}

/**
 * 更新指定项目id和版本号的接口配置数据
 * @param {String} projectId 
 * @param {Array} moduleList 
 * @param {String} version 
 * @param {Function} callback 
 */
function updateData({ projectId, version, moduleList }, callback) {
    Tag.findOne({ projectId: projectId }, (err, result) => {
        if (err) {
            return callback(err)
        }
        if (!result) {
            return callback('查询失败')
        }

        const tagList = _cloneDeep(result.tagList)
        const compareResult = tagList.some(item => {
            if (item.tag === version) {
                item.data = _cloneDeep(moduleList)
                return true
            }
            return false
        })
        if (!compareResult) {
            return callback('没有此版本号')
        }

        Tag.updateOne({ projectId: projectId }, { tagList: tagList }, (err, result) => {
            if (err) {
                return callback('更新失败')
            }
            callback(null)
        })
    })
}

module.exports = {
    getData: getData,
    updateData: updateData
}