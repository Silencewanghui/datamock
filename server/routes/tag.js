const express = require('express');
const router = express.Router();
const Project = require('../models/project')
const Tag = require('../models/tag')
const errorHandler = require('./errorHandler');
const _cloneDeep = require('lodash/cloneDeep')

// 递增版本号
function increaseVersion(curVersion) {
    let versionIndex = Number(curVersion.split('v')[1])
    const curVersionIndex = versionIndex + 1

    return `v${curVersionIndex}`
}

// 更新项目表中对应的tag字段
function updateProjectTag(projectId, version) {
    return new Promise((resolve, reject) => {
        Project.updateOne({ projectId: projectId }, {
            version: version
        }, (err, result) => {
            if (err) {
                return reject(err)
            }
            resolve()
        })
    })
}

/**
 * 新增指定项目的版本
 * @param  {String} projectId [项目ID]
 * @param  {Array} moduleList [接口配置数据]
 */
router.post('/add', (req, res) => {
    const result1 = errorHandler.paramValidate(req.body, 'projectId')
    const result2 = errorHandler.paramValidate(req.body, 'moduleList')

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    } else if (result2) {
        return errorHandler.requestErrorHandler(res, result2, '')
    }

    const projectId = req.body.projectId
    const moduleList = req.body.moduleList

    Tag.findOne({ projectId: projectId }, (err, result) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, '新增版本失败', err)
        }
        // 取出当前项目的最新版本，并递增
        const _version = increaseVersion(result.curTag)
        const tagList = _cloneDeep(result.tagList)

        tagList.push({
            tag: _version,
            data: moduleList
        })

        // 更新数据库
        Tag.updateOne({ projectId: projectId }, {
            tagList: tagList,
            curTag: _version
        }, (err, result) => {
            if (err) {
                return errorHandler.requestErrorHandler(res, '新增版本失败', err)
            }
            updateProjectTag(projectId, _version).then(() => {
                return res.json({
                    status: 0,
                    data: {
                        version: _version
                    }
                })
            }).catch(err => {
                return errorHandler.requestErrorHandler(res, '新增版本失败', err)
            })
        })
    })
})

/**
 * 获取指定项目的版本列表
 * @param  {String} projectId [项目ID]
 */
router.get('/tagList', (req, res) => {
    const result1 = errorHandler.paramValidate(req.query, 'projectId')

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    }

    const projectId = req.query.projectId

    Tag.findOne({ projectId: projectId }, (err, result) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, '查询失败', err)
        }

        if (!result) {
            return errorHandler.requestErrorHandler(res, '没有相关的版本信息', err)
        }

        const tagList = _cloneDeep(result.tagList)
        const versionList = []

        tagList.forEach(item => {
            versionList.push(item.tag)
        })

        return res.json({
            status: 0,
            data: versionList
        })
    })
})

/**
 * 获取指定版本的接口配置数据
 * @param  {String} projectId [项目ID]
 * @param  {String} version [版本号]
 */
router.get('/get', (req, res) => {
    const result1 = errorHandler.paramValidate(req.query, 'projectId')
    const result2 = errorHandler.paramValidate(req.query, 'version')

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    } else if (result2) {
        return errorHandler.requestErrorHandler(res, result2, '')
    }

    const projectId = req.query.projectId
    const version = req.query.version

    getData(projectId, version, (err, data) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, err, '')
        }
        return res.json({
            status: 0,
            data: {
                version: data.tag,
                data: data.data
            }
        })
    })
})


module.exports = {
    router: router
}