module.exports = (app) => {
    app.get('/', (req, res) => {
        res.json({
            message: 'Hello World!'
        });
    });

    app.use('/rest/project', require('./project'));
    app.use('/rest/api', require('./api').router);
    app.use('/rest/tag', require('./tag').router);
}