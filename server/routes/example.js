const example = {
    "projectId": "8974",
    "identifier": "decoder",
    "moduleList": [{
        "name": "",
        "identifier": "",
        "actionList": [{
            "name": "获取轮巡",
            "identifier": "scheduler",
            "requestType": "get",
            "requestParameterList": [{
                "identifier": "id",
                "name": "轮巡id",
                "remark": "",
                "dataType": "string",
                "parameterList": []
            }],
            "responseParameterList": [{
                "identifier": "id",
                "name": "轮巡id",
                "remark": "",
                "dataType": "number",
                "mockType": "random&number",
                "parameterList": []
            }, {
                "identifier": "name",
                "name": "轮巡名称",
                "remark": "",
                "dataType": "string",
                "mockType": "random&word",
                "parameterList": []
            }, {
                "identifier": "state",
                "name": "状态",
                "remark": "",
                "dataType": "number",
                "mockType": "random&number",
                "parameterList": []
            }, {
                "identifier": "interval",
                "name": "轮巡间隔",
                "remark": "",
                "dataType": "number",
                "mockType": "enum",
                "enumData": [5, 10, 15, 20],
                "parameterList": []
            }, {
                "identifier": "monitorList",
                "name": "监视器列表",
                "remark": "",
                "dataType": "array<object>",
                "mockType": "",
                "parameterList": [{
                    "identifier": "monitor",
                    "name": "监视器编号",
                    "remark": "",
                    "dataType": "number",
                    "mockType": "random&number",
                    "parameterList": []
                }, {
                    "identifier": "paneList",
                    "name": "轮巡pane编号",
                    "remark": "",
                    "dataType": "array<number>",
                    "mockType": "random&number",
                    "parameterList": []
                }, {
                    "identifier": "devList",
                    "name": "轮巡的设备",
                    "remark": "",
                    "dataType": "array<object>",
                    "mockType": "",
                    "parameterList": [{
                        "identifier": "devIp",
                        "name": "设备ip",
                        "remark": "",
                        "dataType": "string",
                        "mockType": "internet&ip",
                        "parameterList": []
                    }, {
                        "identifier": "devPort",
                        "name": "设备端口",
                        "remark": "",
                        "dataType": "number",
                        "mockType": "enum",
                        "enumData": [80, 81],
                        "parameterList": []
                    }, {
                        "identifier": "channel",
                        "name": "设备通道",
                        "remark": "",
                        "dataType": "number",
                        "mockType": "random&number",
                        "parameterList": []
                    }, {
                        "identifier": "stream",
                        "name": "码流",
                        "remark": "",
                        "dataType": "number",
                        "mockType": "random&number",
                        "parameterList": []
                    }, {
                        "identifier": "streamPort",
                        "name": "端口号",
                        "remark": "蓝星设备需要此端口号",
                        "dataType": "number",
                        "mockType": "random&number",
                        "parameterList": []
                    }]
                }]
            }]
        }]
    }]
};
module.exports = example;