const express = require('express');
const Tag = require('../../models/tag');
const Project = require('../../models/project');
const Util = require('./util');
const errorHandler = require('../errorHandler')
const GenDynamicRoute = require('../api').genDynamicRoute;
const router = express.Router();

// 新建项目
router.post('/addProject', (req, res) => {
    const result1 = errorHandler.paramValidate(req.body, 'projectId')
    const result2 = errorHandler.paramValidate(req.body, 'identifier')
    const result3 = errorHandler.paramValidate(req.body, 'moduleList')

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    } else if (result2) {
        return errorHandler.requestErrorHandler(res, result2, '')
    } else if (result3) {
        return errorHandler.requestErrorHandler(res, result3, '')
    }

    const projectId = req.body.projectId || ''
    const identifier = req.body.identifier || ''
    const moduleList = req.body.moduleList || []

    // 依据项目的 identifier 进行数据查重
    function compareProject() {
        return new Promise((resolve, reject) => {
            Project.findOne({ identifier: identifier }, (err, project) => {
                if (err) {
                    return reject(err);
                }
                resolve(project);
            });
        })
    }

    compareProject().then(result => {
        if (result) {
            return errorHandler.requestErrorHandler(res, '项目标识重复，请修改后添加', '')
        }

        // moduleList校验
        for (let i = 0; i < moduleList.length; i++) {
            const moduleItem = moduleList[i]
            for (let j = 0; j < moduleItem.actionList.length; j++) {
                const actionItem = moduleItem.actionList[j];
                if (!actionItem.name || !actionItem.identifier || !actionItem.requestType) {
                    return errorHandler.requestErrorHandler(res, '接口配置参数不完整', '')
                }
                // 参数列表校验
                const err1 = Util.identify(actionItem.requestParameterList);
                const err2 = Util.identify(actionItem.responseParameterList);
                const error = err1 || err2;
                if (error) {
                    return errorHandler.requestErrorHandler(res, error, '')
                }
            }
        }

        // 保存至版本表
        // 初始版本为1.0
        const _version = 'v1'
        const tagList = [].concat({
            tag: _version,
            data: moduleList
        })
        const newTag = new Tag({
            projectId: projectId,
            identifier: identifier,
            tagList: tagList,
            curTag: _version
        })
        newTag.save(err => {
            if (err) {
                return errorHandler.requestErrorHandler(res, '新增项目版本失败', err)
            }
            // 保存至数据库
            const project = new Project({
                projectId: projectId,
                identifier: identifier,
                version: _version
            })

            project.save(err => {
                if (err) {
                    return errorHandler.requestErrorHandler(res, '新建项目失败', err)
                }

                return res.json({
                    data: {
                        message: '接口配置成功'
                    },
                    status: 0
                })
            })
        })
    }).catch(e => {
        return errorHandler.requestErrorHandler(res, '系统错误', e)
    })
})

module.exports = router;