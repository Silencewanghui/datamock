// 参数列表校验
function identify(parameterList) {
    let err = '';
    for (let k = 0; k < parameterList.length; k++) {
        const parameterItem = parameterList[k];

        if (!parameterItem.identifier || !parameterItem.dataType) {
            err = 'parameterList参数不完整';
        }

        if (parameterItem.parameterList && parameterItem.parameterList.length) {
            err = identify(parameterItem.parameterList);
        }
    }
    return err;
}

module.exports = {
    identify: identify
}