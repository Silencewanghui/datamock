// 请求参数验证

// 参数对象遍历
function loopObj(obj, lastRoute, handleFunc) {
    for (let key in obj) {
        const result = handleFunc(key, obj[key], lastRoute);
        if (result.next) {
            loopObj(result.next, result.lastRoute, handleFunc);
        }
    }
}

// 返回对应参数的数据类型
function getDataType(routes, rules) {
    if (routes.indexOf('.') === -1) {
        return rules[routes].dataType;
    }

    let routesArr = routes.split('.');
    let content = null;
    while (routesArr.length) {
        const routeItem = routesArr.shift();
        if (content === null) {
            content = rules[routeItem];
        } else {
            content = content.child[routeItem];
        }
    }

    return content.dataType;
}

function requestParaValidate(requestPara, rules) {
    let route = '';
    const errors = [];

    loopObj(requestPara, route, (key, item, lastRoute) => {
        const route = `${lastRoute}.${key}`;
        const dataType = getDataType(route.slice(1), rules);
        let tempItem = null

        try {
            if (dataType === 'number') {
                tempItem = Number(item[0])
            } else if (dataType === 'boolean') {
                tempItem = Boolean(item[0])
            } else if (dataType === 'string') {
                tempItem = String(item[0])
            }
        } catch (err) {
            console.error(err)
        }

        const arrayDataType = ['array<number>', 'array<string>',
            'array<boolean>', 'array<object>', 'array<array>'
        ];

        const result = arrayDataType.some(type => type === dataType)

        if (result) {
            if (dataType.indexOf(typeof item[0]) === -1) {
                errors.push(`${route.slice(1)}数据类型错误`);
            }
        } else if (typeof tempItem !== dataType) {
            errors.push(`${route.slice(1)}数据类型错误`);
        }

        return {
            next: typeof item === 'object' ? item : null,
            lastRoute: route
        }
    });

    return errors;
}

module.exports = {
    requestParaValidate: requestParaValidate
}