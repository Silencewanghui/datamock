const HandleParameter = require('./handleAPIConfig').handleParameter
const GenerateMockData = require('./generateMockData').generateMockData

// 获取路由生成所需要的数据
function getRouteRules({ identifier, moduleList, version }) {
    const actionList = []
    const routeList = []

    while (moduleList.length) {
        const moduleItem = moduleList.shift()
        const moduleIdentifier = moduleItem.identifier
        while (moduleItem.actionList.length) {
            const actionItem = moduleItem.actionList.shift()
            const actionIdentifier = actionItem.identifier
            const actionRequestType = actionItem.requestType

            // 请求参数验证规则
            const requestParaObj = HandleParameter(actionItem.requestParameterList)

            // 数据模拟规则
            const responseParaObj = HandleParameter(actionItem.responseParameterList)

            actionList.push({
                identifier: identifier,
                moduleIdentifier: moduleIdentifier,
                actionIdentifier: actionIdentifier,
                requestType: actionRequestType,
                requestParaRules: requestParaObj,
                responseParaRules: responseParaObj
            })
        }
    }

    while (actionList.length) {
        const actionItem = actionList.shift()

        // 生成接口地址
        let url = ''
        if (actionItem.moduleIdentifier) {
            url = `/${actionItem.identifier}/${version}/${actionItem.moduleIdentifier}/${actionItem.actionIdentifier}`
        } else {
            url = `/${actionItem.identifier}/${version}/${actionItem.actionIdentifier}`
        }

        routeList.push({
            url: url,
            requestType: actionItem.requestType,
            requestParaRules: actionItem.requestParaRules,
            data: GenerateMockData(actionItem.responseParaRules) // 模拟数据
        })
    }

    return routeList
}

module.exports = {
    getRouteRules: getRouteRules
}