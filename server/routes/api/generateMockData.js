const faker = require('faker');

// 遍历返回数据配置
function loopRules(obj, parentObj, handleFunc) {
    for (let key in obj) {
        const result = handleFunc(key, obj[key], parentObj);
        if (result.next) {
            // 默认给数据类型为array<object>、array<array>，十条数据
            if (result.isArray_object) {
                for (let i = 0; i < 10; i++) {
                    result.list[i] = {}
                    loopRules(result.next, result.list[i], handleFunc);
                }
            } else if (result.isArray_array) {
                for (let i = 0; i < 10; i++) {
                    result.list[i] = []
                    loopRules(result.next, result.list[i], handleFunc);
                }
            } else {
                loopRules(result.next, result.parentObj, handleFunc);
            }
        }
    }
}

// 数据类型为 string number boolean 时，生成模拟数据
function createData(type, mockType) {
    if (mockType) {
        return faker[mockType.split('&')[0]][mockType.split('&')[1]]()
    }

    if (type === 'boolean') {
        return faker.random.boolean()
    }

    if (type === 'string') {
        return faker.random.word()
    }

    if (type === 'number') {
        return faker.random.number()
    }
}

// 生成子数据
function createChildData(dataType, mockType) {
    let result = null;
    let isBaseType = false;

    if (dataType === 'string' || dataType === 'number' || dataType === 'boolean') {
        result = createData(dataType, mockType);
        isBaseType = true;
    } else if (dataType === 'object') {
        result = {};
    } else if (dataType === 'array<object>') {
        result = [{}];
    } else if (dataType === 'array<array>') {
        result = [
            []
        ];
    } else if (dataType === 'array<number>' || dataType === 'array<string>' || dataType === 'array<boolean>') {
        result = [].push(createData(dataType.split('<')[1].slice(0, -1), mockType));
        isBaseType = true;
    }

    return {
        isBaseType: isBaseType,
        data: result
    }
}

// 生成模拟数据
function generateMockData(rules) {
    const mockData = {};

    loopRules(rules, mockData, (key, item, parentObj) => {
        let parentType = '',
            result;

        if (Array.isArray(parentObj)) {
            parentType = 'array';
        } else if (typeof parentObj === 'object') {
            parentType = 'object';
        } else {
            parentType = typeof parentObj;
        }

        if (item.mockType === 'enum') {
            const randomIndex = Math.floor(Math.random() * item.enumData.length)
            const mockData = item.enumData[randomIndex]
            result = {
                isBaseType: true,
                data: mockData
            }
        } else {
            result = createChildData(item.dataType, item.mockType);
        }

        if (parentType === 'array') {
            parentObj.push(result.data)
        }

        if (parentType === 'object') {
            parentObj[key] = result.data;
        }

        if (result.isBaseType) {
            return {
                next: null,
                parentObj: null
            }
        }

        return {
            next: item.child,
            parentObj: item.dataType === 'array<object>' || item.dataType === 'array<array>' ?
                parentObj[key][0] : parentObj[key],
            isArray_object: item.dataType === 'array<object>',
            isArray_array: item.dataType === 'array<array>',
            list: parentObj[key]
        }
    })

    return mockData;
}

module.exports = {
    generateMockData: generateMockData
}