// 遍历 请求/返回 参数列表
function loopTree(list, lastRoute, handleFunc) {
    list.forEach(item => {
        const result = handleFunc(item, lastRoute)
        if (result.next) {
            loopTree(result.next, result.lastRoute, handleFunc)
        }
    })
}

// 形成 请求/返回 参数对象结构
function handleParaRoutes(parameterObj, paraRoutes) {
    paraRoutes.forEach(item => {
        let routeList = item.split('|')
        let current = parameterObj

        while (routeList.length) {
            const routeItem = routeList.shift()
            const identifier = routeItem.split('.')[0]
            const dataType = routeItem.split('.')[1]

            if (!current[identifier]) {
                current[identifier] = {
                    dataType: dataType,
                    mockType: routeItem.split('.')[2] ? routeItem.split('.')[2] : '',
                    child: {}
                }
                if (current[identifier].mockType === 'enum') {
                    current[identifier].enumData = enumObj[item]
                }
            }
            current = current[identifier].child
        }
    })
    return parameterObj
}

// 处理参数列表
let enumObj = {} // 枚举类型集合
function handleParameter(parameterList) {
    let parameterObj = {}, // 参数对象
        paraRoutes = [] // 参数路径

    while (parameterList.length) {
        const parameterItem = parameterList.shift()
        const routesList = []
        let routes = `${parameterItem.identifier}.${parameterItem.dataType}|`

        if (parameterItem.mockType && parameterItem.mockType !== '') {
            routes = `${parameterItem.identifier}.${parameterItem.dataType}.${parameterItem.mockType}|`
            if (parameterItem.mockType === 'enum') {
                const key = routes.slice(0, routes.length - 1)
                enumObj[key] = parameterItem.enumData ? parameterItem.enumData : []
            }
        }

        if (parameterItem.parameterList.length) {
            loopTree(parameterItem.parameterList, routes, (item, lastRoute) => {
                if (item.mockType && item.mockType !== '') {
                    routes = lastRoute + `${item.identifier}.${item.dataType}.${item.mockType}|`
                    if (item.mockType === 'enum') {
                        const key = routes.slice(0, routes.length - 1)
                        enumObj[key] = item.enumData ? item.enumData : []
                    }
                } else {
                    routes = lastRoute + `${item.identifier}.${item.dataType}|`
                }


                if (!item.parameterList.length) {
                    routesList.push(routes.slice(0, routes.length - 1))
                }

                return {
                    next: item.parameterList.length ? item.parameterList : null,
                    lastRoute: routes
                }
            })
        } else {
            routesList.push(routes.slice(0, routes.length - 1))
        }

        paraRoutes.push(routesList)
    }

    paraRoutes.forEach(item => {
        parameterObj = handleParaRoutes(parameterObj, item)
    })

    return parameterObj
}

module.exports = {
    handleParameter: handleParameter
}