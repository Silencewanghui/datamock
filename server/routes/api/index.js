const express = require('express');
const router = express.Router();
const Tag = require('../../models/tag');
const Project = require('../../models/project');
const _cloneDeep = require('lodash/cloneDeep');
const errorHandler = require('../errorHandler');
const genDynamicRoute = require('../mockApi').genDynamicRoute;
const getTagData = require('../tagUtil').getData;
const updateTagData = require('../tagUtil').updateData;

/**
 * 开启某个项目的 mock 服务
 * @param  {String} projectId [项目ID]
 * @param  {String} identifier [项目标志]
 * @param  {String} version [版本号]
 */
router.post('/startAPI', (req, res) => {
    const result1 = errorHandler.paramValidate(req.body, 'projectId')
    const result2 = errorHandler.paramValidate(req.body, 'identifier')
    const result3 = errorHandler.paramValidate(req.body, 'version')

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    } else if (result2) {
        return errorHandler.requestErrorHandler(res, result2, '')
    } else if (result3) {
        return errorHandler.requestErrorHandler(res, result3, '')
    }

    const projectId = req.body.projectId || ''
    const identifier = req.body.identifier || ''
    const version = req.body.version || ''

    getTagData(projectId, version, (err, data) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, err, '')
        }

        const tag = data.tag
        const moduleList = data.data

        // 生成动态路由
        genDynamicRoute({
            identifier: identifier,
            moduleList: moduleList,
            version: version
        })

        setTimeout(() => {
            return res.json({
                status: 0,
                message: 'mock服务开启成功'
            })
        }, 0)
    })
})

/**
 * 获取接口配置详情
 * @param  {String} projectId [项目ID]
 * @param  {String} version [版本号]
 */
router.get('/api', (req, res) => {
    const result1 = errorHandler.paramValidate(req.query, 'projectId')
    const result2 = errorHandler.paramValidate(req.query, 'version')

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    } else if (result2) {
        return errorHandler.requestErrorHandler(res, result2, '')
    }

    const projectId = req.query.projectId
    const version = req.query.version

    getTagData(projectId, version, (err, data) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, err, '')
        }
        if (!data) {
            return errorHandler.requestErrorHandler(res, '查询失败', '')
        }

        const tag = data.tag
        const moduleList = data.data

        Project.findOne({ projectId: projectId }, (err, result) => {
            if (err || !result) {
                return errorHandler.requestErrorHandler(res, '查询失败', '')
            }
            return res.json({
                data: {
                    data: {
                        projectId: projectId,
                        identifier: result.identifier,
                        tag: tag,
                        moduleList: moduleList
                    }
                },
                status: 0
            })
        })
    })
})

/**
 * 修改接口配置参数
 * @param  {String} projectId [项目ID]
 * @param  {String} version [版本号]
 * @param  {Object} originIdentifier [原有标识]
 * @param  {Object} updatedData [更改后的配置数据]
 * {
 *  projectId: xxx,
 *  version: xxx,
 *  originIdentifier: {
 *   moduleIdentifier: xxx,
 *   actionIdentifier: xxx
 *  },
 *  updatedData: {
 *   name: xxx,
 *   identifier: xxx,
 *   requestType: xxx,
 *   requestParameterList: [],
 *   responseParameterList: []
 *  }
 * }
 */
router.post('/updateApi', (req, res) => {
    const result1 = errorHandler.paramValidate(req.body, 'projectId')
    const result2 = errorHandler.paramValidate(req.body, 'version')
    const result3 = errorHandler.paramValidate(req.body, 'originIdentifier', ['moduleIdentifier', 'actionIdentifier'])
    const result4 = errorHandler.paramValidate(req.body, 'updatedData', ['name', 'identifier', 'requestType', 'requestParameterList', 'responseParameterList'])

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    } else if (result2) {
        return errorHandler.requestErrorHandler(res, result2, '')
    } else if (result3) {
        return errorHandler.requestErrorHandler(res, result3, '')
    } else if (result4) {
        return errorHandler.requestErrorHandler(res, result4, '')
    }

    const projectId = req.body.projectId
    const version = req.body.version
    const originIdentifier = req.body.originIdentifier
    const updatedData = req.body.updatedData

    let originModuleIdentifier = originIdentifier.moduleIdentifier,
        originActionIdentifier = originIdentifier.actionIdentifier

    getTagData(projectId, version, (err, data) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, err, '')
        }
        if (!data) {
            return errorHandler.requestErrorHandler(res, '查询失败', '')
        }

        const moduleList = data.data

        let moduleIndex = null,
            actionIndex = null

        moduleIndex = moduleList.findIndex(item => item.identifier === originModuleIdentifier)
        if (moduleIndex === -1) {
            return errorHandler.requestErrorHandler(res, '不存在该模块', '')
        }
        actionIndex = moduleList[moduleIndex].actionList.findIndex(item => {
            return item.identifier === originActionIdentifier
        })
        if (actionIndex === -1) {
            return errorHandler.requestErrorHandler(res, '不存在该接口', '')
        }

        // actionIdentifier 判重
        if (updatedData.identifier !== originActionIdentifier) {
            const result = moduleList[moduleIndex].actionList.some(item => {
                if (item.identifier === updatedData.identifier) {
                    return true
                }
                return false
            })
            if (result) {
                return errorHandler.requestErrorHandler(res, '新的接口标识重复', '')
            }
        }

        // 更新数据
        moduleList[moduleIndex].actionList[actionIndex] = updatedData

        updateTagData({ projectId, version, moduleList }, err => {
            if (err) {
                return errorHandler.requestErrorHandler(res, err, '')
            }
            return res.json({
                status: 0,
                data: {
                    message: '更新成功'
                }
            })
        })
    })
})

/**
 * 修改模块信息
 * @param  {String} projectId [项目ID]
 * @param  {String} version [版本号]
 * @param  {Object} originData [原有信息]
 * @param  {Object} updatedData [更改后的数据]
 * {
 *  projectId: xxx,
 *  version: xxx,
 *  originData: {
 *   name: xxx,
 *   identifier: xxx
 *  },
 *  updatedData: {
 *   name: xxx,
 *   identifier: xxx
 *  }
 * }
 */
router.post('/updateModule', (req, res) => {
    const result1 = errorHandler.paramValidate(req.body, 'projectId')
    const result2 = errorHandler.paramValidate(req.body, 'version')
    const result3 = errorHandler.paramValidate(req.body, 'originData', ['name', 'identifier'])
    const result4 = errorHandler.paramValidate(req.body, 'updatedData', ['name', 'identifier'])

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    } else if (result2) {
        return errorHandler.requestErrorHandler(res, result2, '')
    } else if (result3) {
        return errorHandler.requestErrorHandler(res, result3, '')
    } else if (result4) {
        return errorHandler.requestErrorHandler(res, result4, '')
    }

    const projectId = req.body.projectId
    const version = req.body.version
    const originData = req.body.originData
    const updatedData = req.body.updatedData

    getTagData(projectId, version, (err, data) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, err, '')
        }
        if (!data) {
            return errorHandler.requestErrorHandler(res, '查询失败', '')
        }

        const moduleList = _cloneDeep(data.data)

        let moduleIndex = null

        moduleIndex = moduleList.findIndex(item => item.identifier === originData.identifier)
        if (moduleIndex === -1) {
            return errorHandler.requestErrorHandler(res, '不存在该模块', '')
        }

        // moduleIdentifier 判重
        const result = moduleList.some((item, index) => {
            return (item.identifier === updatedData.identifier || item.name === updatedData.name) &&
                index !== moduleIndex
        })
        if (result) {
            return errorHandler.requestErrorHandler(res, '新的模块标识或名称重复', '')
        }

        // 更新数据
        moduleList[moduleIndex].name = updatedData.name
        moduleList[moduleIndex].identifier = updatedData.identifier

        updateTagData({ projectId, version, moduleList }, err => {
            if (err) {
                return errorHandler.requestErrorHandler(res, err, '')
            }
            return res.json({
                status: 0,
                data: {
                    message: '更新模块信息成功'
                }
            })
        })
    })
})

/**
 * 新增模块
 * @param  {String} projectId [项目ID]
 * @param  {String} version [版本号]
 * @param  {Object} moduleData [模块信息]
 * {
 *  projectId: xxx,
 *  version: xxx,
 *  moduleData: {
 *   name: xxx,
 *   identifier: xxx
 *  }
 * }
 */
router.post('/addModule', (req, res) => {
    const result1 = errorHandler.paramValidate(req.body, 'projectId')
    const result2 = errorHandler.paramValidate(req.body, 'version')
    const result3 = errorHandler.paramValidate(req.body, 'moduleData', ['name', 'identifier'])

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    } else if (result2) {
        return errorHandler.requestErrorHandler(res, result2, '')
    } else if (result3) {
        return errorHandler.requestErrorHandler(res, result3, '')
    }

    const projectId = req.body.projectId
    const version = req.body.version
    const moduleData = req.body.moduleData

    getTagData(projectId, version, (err, data) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, err, '')
        }
        if (!data) {
            return errorHandler.requestErrorHandler(res, '查询失败', '')
        }

        const moduleList = _cloneDeep(data.data)

        if (data.data.length) {
            let moduleIndex = null

            moduleIndex = moduleList.findIndex(item => item.identifier === originData.identifier)
            if (moduleIndex === -1) {
                return errorHandler.requestErrorHandler(res, '不存在该模块', '')
            }

            // moduleIdentifier 判重
            const result = moduleList.some((item, index) => {
                return (item.identifier === updatedData.identifier || item.name === updatedData.name) &&
                    index !== moduleIndex
            })
            if (result) {
                return errorHandler.requestErrorHandler(res, '新的模块标识或名称重复', '')
            }
        }

        moduleList.push({
            actionList: [],
            identifier: moduleData.identifier,
            name: moduleData.name
        })

        // 更新数据
        updateTagData({ projectId, version, moduleList }, err => {
            if (err) {
                return errorHandler.requestErrorHandler(res, err, '')
            }
            return res.json({
                status: 0,
                data: {
                    message: '新增模块成功'
                }
            })
        })
    })
})

/**
 * 新增接口
 * @param  {String} projectId [项目ID]
 * @param  {String} version [版本号]
 * @param  {Object} moduleData [模块信息]
 * @param  {Object} apiData [接口信息]
 * {
 *  projectId: xxx,
 *  version: xxx,
 *  moduleData: {
 *   identifier: xxx
 *  },
 *  apiData: {
 *   name: xxx,
 *   identifier: xxx,
 *   requestType: xxx
 *  }
 * }
 */
router.post('/addApi', (req, res) => {
    const result1 = errorHandler.paramValidate(req.body, 'projectId')
    const result2 = errorHandler.paramValidate(req.body, 'version')
    const result3 = errorHandler.paramValidate(req.body, 'moduleData', ['identifier'])
    const result4 = errorHandler.paramValidate(req.body, 'apiData', ['name', 'identifier', 'requestType'])

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    } else if (result2) {
        return errorHandler.requestErrorHandler(res, result2, '')
    } else if (result3) {
        return errorHandler.requestErrorHandler(res, result3, '')
    } else if (result4) {
        return errorHandler.requestErrorHandler(res, result4, '')
    }

    const projectId = req.body.projectId
    const version = req.body.version
    const moduleData = req.body.moduleData
    const apiData = req.body.apiData

    getTagData(projectId, version, (err, data) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, err, '')
        }
        if (!data) {
            return errorHandler.requestErrorHandler(res, '查询失败', '')
        }

        const moduleList = _cloneDeep(data.data)

        let moduleIndex = null

        moduleIndex = moduleList.findIndex(item => item.identifier === moduleData.identifier)
        if (moduleIndex === -1) {
            return errorHandler.requestErrorHandler(res, '不存在该模块', '')
        }

        const apiIndex = moduleList[moduleIndex].actionList.findIndex(item => {
            return item.identifier === apiData.identifier || item.name === apiData.name
        })
        if (apiIndex !== -1) {
            return errorHandler.requestErrorHandler(res, '新的接口标识或名称重复', '')
        }

        moduleList[moduleIndex].actionList.push(
            Object.assign({}, apiData, {
                requestParameterList: [],
                responseParameterList: []
            })
        )

        // 更新数据
        updateTagData({ projectId, version, moduleList }, err => {
            if (err) {
                return errorHandler.requestErrorHandler(res, err, '')
            }
            return res.json({
                status: 0,
                data: {
                    message: '新增接口成功'
                }
            })
        })
    })
})

/**
 * 删除模块
 * @param  {String} projectId [项目ID]
 * @param  {String} version [版本号]
 * @param  {Object} moduleData [模块信息]
 * {
 *  projectId: xxx,
 *  version: xxx,
 *  moduleData: {
 *   identifier: xxx
 *  }
 * }
 */
router.post('/delModule', (req, res) => {
    const result1 = errorHandler.paramValidate(req.body, 'projectId')
    const result2 = errorHandler.paramValidate(req.body, 'version')
    const result3 = errorHandler.paramValidate(req.body, 'moduleData', ['identifier'])

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    } else if (result2) {
        return errorHandler.requestErrorHandler(res, result2, '')
    } else if (result3) {
        return errorHandler.requestErrorHandler(res, result3, '')
    }

    const projectId = req.body.projectId
    const version = req.body.version
    const moduleData = req.body.moduleData

    getTagData(projectId, version, (err, data) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, err, '')
        }
        if (!data) {
            return errorHandler.requestErrorHandler(res, '查询失败', '')
        }

        const moduleList = _cloneDeep(data.data)

        let moduleIndex = null

        moduleIndex = moduleList.findIndex(item => item.identifier === moduleData.identifier)
        if (moduleIndex === -1) {
            return errorHandler.requestErrorHandler(res, '不存在该模块', '')
        }

        // 删除该模块
        moduleList.splice(moduleIndex, 1)

        // 更新数据
        updateTagData({ projectId, version, moduleList }, err => {
            if (err) {
                return errorHandler.requestErrorHandler(res, err, '')
            }
            return res.json({
                status: 0,
                data: {
                    message: '删除模块成功'
                }
            })
        })
    })
})

/**
 * 删除接口
 * @param  {String} projectId [项目ID]
 * @param  {String} version [版本号]
 * @param  {Object} moduleData [模块信息]
 * @param  {Object} apiData [接口信息]
 * {
 *  projectId: xxx,
 *  version: xxx,
 *  moduleData: {
 *   identifier: xxx
 *  },
 *  apiData: {
 *   identifier: xxx
 *  }
 * }
 */
router.post('/delApi', (req, res) => {
    const result1 = errorHandler.paramValidate(req.body, 'projectId')
    const result2 = errorHandler.paramValidate(req.body, 'version')
    const result3 = errorHandler.paramValidate(req.body, 'moduleData', ['identifier'])
    const result4 = errorHandler.paramValidate(req.body, 'apiData', ['identifier'])

    if (result1) {
        return errorHandler.requestErrorHandler(res, result1, '')
    } else if (result2) {
        return errorHandler.requestErrorHandler(res, result2, '')
    } else if (result3) {
        return errorHandler.requestErrorHandler(res, result3, '')
    } else if (result4) {
        return errorHandler.requestErrorHandler(res, result4, '')
    }

    const projectId = req.body.projectId
    const version = req.body.version
    const moduleData = req.body.moduleData
    const apiData = req.body.apiData

    getTagData(projectId, version, (err, data) => {
        if (err) {
            return errorHandler.requestErrorHandler(res, err, '')
        }
        if (!data) {
            return errorHandler.requestErrorHandler(res, '查询失败', '')
        }

        const moduleList = _cloneDeep(data.data)

        let moduleIndex = null

        moduleIndex = moduleList.findIndex(item => item.identifier === moduleData.identifier)
        if (moduleIndex === -1) {
            return errorHandler.requestErrorHandler(res, '不存在该模块', '')
        }

        const apiIndex = moduleList[moduleIndex].actionList.findIndex(item => {
            return item.identifier === apiData.identifier
        })
        if (apiIndex === -1) {
            return errorHandler.requestErrorHandler(res, '不存在该接口', '')
        }
        // 删除
        moduleList[moduleIndex].actionList.splice(apiIndex, 1)

        // 更新数据
        updateTagData({ projectId, version, moduleList }, err => {
            if (err) {
                return errorHandler.requestErrorHandler(res, err, '')
            }
            return res.json({
                status: 0,
                data: {
                    message: '删除接口成功'
                }
            })
        })
    })
})

module.exports = {
    router: router
}