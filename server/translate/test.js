const translate = require('./translate')
const fakeAPI = require('./fakeAPI')

const resultObj = {}
const promiseArr = []

for (const type in fakeAPI) {
    let methodNameArr = fakeAPI[type]
    methodNameArr.forEach(method => {
        const requestPromise = new Promise((resolve, reject) => {
            translate(method).then(result => {
                if (!resultObj[type]) {
                    resultObj[type] = {}
                }
                resultObj[type][method] = {
                    methodName: method,
                    zh_CHS: result.result.length ? result.result[0] : [],
                    mockRule: `${type}&${method}`
                }
                resolve()
            }).catch(err => {
                reject(err)
            })
        })
        promiseArr.push(requestPromise)
    })
}

Promise.all(promiseArr).then(() => {
    console.log('翻译完成')
    console.log(JSON.stringify(resultObj))
}).catch(err => {
    console.error(err)
})