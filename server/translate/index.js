/**
 * 翻译模块（有道翻译的API）
 * 接受需要翻译的参数
 */
const md5 = require('md5')
const request = require('request')

const from = 'auto',
    to = 'auto',
    appKey = '5d90a966110b3e2c',
    key = 'pS1gasiovHN8JyJgRnOwWzSjjxODl0al',
    salt = Math.floor(Math.random() * 10)

function translate(query) {
    const q = query
    const sign = md5(appKey + q + salt + key)
    const encodeQueryText = encodeURIComponent(q)
    const url = 'http://openapi.youdao.com/api?q=' + encodeQueryText +
        '&from=' + from + '&to=' + to + '&appKey=' + appKey +
        '&salt=' + salt + '&sign=' + sign

    return new Promise((resolve, reject) => {
        request.get(url, (err, response, body) => {
            if (err) {
                reject(err)
                return
            }
            if (JSON.parse(body).errorCode !== '0') {
                reject(`错误码：${JSON.parse(body).errorCode}`)
                return
            }
            resolve({
                query: query,
                result: JSON.parse(body).translation
            })
        })
    })
}

module.exports = translate