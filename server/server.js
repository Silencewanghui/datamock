const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');
const routes = require('./routes');
const registerMockRoute = require('./routes/mockApi').registerMockRoute
const config = require('./config');

let port = process.env.PORT || 3004;

// 解决跨域访问
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "content-type");
    next();
});

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('webApp'));

routes(app);
registerMockRoute(app);

mongoose.Promise = global.Promise;
mongoose.connect(config.database, {
    useMongoClient: true
});

app.listen(port, () => {
    console.log(`Server is listening at port ${port}`);
});