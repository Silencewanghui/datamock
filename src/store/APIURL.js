export default {
    addProject: 'project/addProject', // 新建项目接口配置数据
    startAPI: 'api/startAPI', // 开启某个项目的mock服务
    api: 'api/api', // 获取某个项目的接口配置数据
    updateApi: 'api/updateApi', // 修改某个接口的配置数据
    updateModule: 'api/updateModule', // 修改模块信息
    addApi: 'api/addApi', // 添加接口
    addModule: 'api/addModule', // 添加模块
    delModule: 'api/delModule', // 删除模块
    delApi: 'api/delApi', // 删除接口,
    tagList: 'tag/tagList', // 获取项目的版本列表
    addVersion: 'tag/add' // 新增项目的一个版本
}