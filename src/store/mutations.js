export default {
    SHOW_LOADING(state) {
        state.loadingState = true
    },
    HIDE_LOADING(state) {
        state.loadingState = false
    },
    SET_PROJECT_ID(state, payload) {
        state.projectId = payload.id
    },
    SET_PROJECT_INFO(state, payload) {
        if (payload.name) {
            state.projectName = payload.name
        } else if (payload.identifier) {
            state.projectIdentifier = payload.identifier
        }
    },
    SET_PROJECT_API_CONFIG(state, payload) {
        state.projectApiData = payload.data
        state.projectIdentifier = payload.data.identifier
        state.version = payload.data.tag
    },
    UPDATE_MODULE(state, payload) {
        const originData = payload.originData
        const updateData = payload.updateData
        const moduleList = state.projectApiData.moduleList

        moduleList.map(item => {
            if (item.name === originData.name && item.identifier === originData.identifier) {
                item.name = updateData.name
                item.identifier = updateData.identifier
            }
            return item
        })
        state.projectApiData = Object.assign({}, state.projectApiData)
    },
    GET_PROJECT_VERSIONLIST(state, payload) {
        state.versionList = payload.data
    },
    SET_PROJECT_VERSION(state, payload) {
        state.version = payload.version
    }
}