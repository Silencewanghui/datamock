import Ajax from '../Ajax'

export default {
    // 显示加载动画
    showLoading({ commit }) {
        commit('SHOW_LOADING');
    },
    // 隐藏加载动画
    hideLoading({ commit }) {
        commit('HIDE_LOADING');
    },
    // 获取所有的项目类型
    getAllProjectTypes({ commit, state }) {
        return new Promise((resolve, reject) => {
            Ajax.getAllProjectTypes((err, data) => {
                if (err) {
                    return reject(err)
                }
                resolve(data)
            })
        })
    },
    // 根据项目类型获取项目列表
    getProjectWithType({ commit, state }, payload) {
        const type = payload.type || ''
        return new Promise((resolve, reject) => {
            Ajax.getProjectWithType(type, (err, data) => {
                if (err) {
                    return reject(err)
                }
                resolve(data)
            })
        })
    },
    // 根据项目id获取项目详情
    getDetailWithId({ commit, state }, payload) {
        const projectId = payload.projectId || ''
        return new Promise((resolve, reject) => {
            Ajax.getDetailWithId(projectId, (err, data) => {
                if (err) {
                    return reject(err)
                }
                resolve(data)
            })
        })
    },
    // 设置项目id
    setProjectId({ commit, state }, payload) {
        commit('SET_PROJECT_ID', { id: payload.id })
    },
    // 设置项目信息
    setProjectInfo({ commit, state }, payload) {
        commit('SET_PROJECT_INFO', payload)
    },
    // 新建项目
    addProject({ commit, state }) {
        return new Promise((resolve, reject) => {
            Ajax.addProject({
                projectId: state.projectId,
                identifier: state.projectIdentifier,
                moduleList: []
            }, (err, data) => {
                if (err) {
                    return reject(err)
                }
                commit('SET_PROJECT_API_CONFIG', {
                    data: {
                        identifier: state.projectIdentifier,
                        moduleList: [],
                        projectId: state.projectId
                    }
                })
                resolve(data.data)
            })
        })
    },
    // 获取项目的接口配置数据
    getProjectApiConfig({ commit, state }) {
        return new Promise((resolve, reject) => {
            Ajax.api(state.projectId, state.version, (err, data) => {
                if (err) {
                    return reject(err)
                }
                commit('SET_PROJECT_API_CONFIG', { data: data.data })
                resolve(data.data)
            })
        })
    },
    // 修改接口的配置数据
    async updateApi({ dispatch, state }, payload) {
        payload.projectId = state.projectId
        payload.version = state.version
        let _data = null

        // 提交至后台
        await new Promise((resolve, reject) => {
            Ajax.updateApi({...payload }, (err, data) => {
                if (err) {
                    return reject(err)
                }
                _data = data
                resolve(data)
            })
        })

        // 重新开启mock服务
        await dispatch('startAPI')

        // 重新获取接口配置数据
        await dispatch('getProjectApiConfig')

        return _data
    },
    // 获取某个接口的配置数据
    getSomeApiData({ state }, payload) {
        const moduleIdentifier = payload.moduleIdentifier
        const apiIdentifier = payload.apiIdentifier
        const moduleList = state.projectApiData.moduleList
        let moduleIndex = NaN
        let apiIndex = NaN

        moduleIndex = moduleList.findIndex(moduleItem => {
            return moduleItem.identifier === moduleIdentifier
        })

        apiIndex = moduleList[moduleIndex].actionList.findIndex(apiItem => apiItem.identifier === apiIdentifier)

        return moduleList[moduleIndex].actionList[apiIndex]
    },
    // 接口identifier校验
    apiIdentifierVerify({ state }, payload) {
        const moduleIdentifier = payload.moduleIdentifier
        const apiIdentifier = payload.apiIdentifier
        const moduleList = state.projectApiData.moduleList
        let moduleIndex = NaN
        let moduleItem = null
        let result = false

        moduleIndex = moduleList.findIndex(moduleItem => {
            return moduleItem.identifier === moduleIdentifier
        })
        moduleItem = moduleList[moduleIndex]

        result = moduleItem.actionList.some(actionItem => {
            return actionItem.identifier === apiIdentifier
        })

        return result
    },
    // 开启某个API的mock服务
    startAPI({ state }) {
        return new Promise((resolve, reject) => {
            Ajax.startAPI({
                projectId: state.projectId,
                identifier: state.projectIdentifier,
                version: state.version
            }, (err, data) => {
                if (err) {
                    return reject(err)
                }
                resolve(data)
            })
        })
    },
    // mock服务预览
    mockPreview({}, payload) {
        return new Promise((resolve, reject) => {
            const {
                URL,
                requestType,
                requestParam
            } = payload

            Ajax.mockPreview(URL, requestType, requestParam,
                (err, data) => {
                    if (err) {
                        return reject(err)
                    }
                    resolve(data)
                })
        })
    },
    // 更改模块信息
    updateModule({ commit, state }, payload) {
        const originData = payload.originData
        const updateData = payload.updateData

        return new Promise((resolve, reject) => {
            Ajax.updateModule(state.projectId, state.version, originData, updateData,
                (err, data) => {
                    if (err) {
                        return reject(err)
                    }
                    resolve(data)
                    commit('UPDATE_MODULE', {
                        originData: originData,
                        updateData: updateData
                    })
                })
        })
    },
    // 新增接口
    async addApi_ajax({ dispatch, state }, payload) {
        const moduleData = payload.moduleData
        const apiData = payload.apiData
        let _data = null

        await new Promise((resolve, reject) => {
            Ajax.addApi(state.projectId, state.version, moduleData, apiData, (err, data) => {
                if (err) {
                    return reject(err)
                }
                _data = data
                resolve(data)
            })
        })

        // 重新获取接口配置数据
        await dispatch('getProjectApiConfig')

        return _data
    },
    // 新增模块
    async addModule_ajax({ dispatch, state }, payload) {
        const moduleData = payload.moduleData
        let _data = null

        await new Promise((resolve, reject) => {
            Ajax.addModule(state.projectId, state.version, moduleData, (err, data) => {
                if (err) {
                    return reject(err)
                }
                _data = data
                resolve(data)
            })
        })

        // 重新获取接口配置数据
        await dispatch('getProjectApiConfig')

        return _data
    },
    // 删除模块
    async delModule_ajax({ dispatch, state }, payload) {
        const moduleData = payload.moduleData
        let _data = null

        await new Promise((resolve, reject) => {
            Ajax.delModule(state.projectId, state.version, moduleData, (err, data) => {
                if (err) {
                    return reject(err)
                }
                _data = data
                resolve(data)
            })
        })

        // 重新获取接口配置数据
        await dispatch('getProjectApiConfig')

        return _data
    },
    // 删除接口
    async delApi_ajax({ dispatch, state }, payload) {
        const moduleData = payload.moduleData
        const apiData = payload.apiData
        let _data = null

        await new Promise((resolve, reject) => {
            Ajax.delApi(state.projectId, state.version, moduleData, apiData, (err, data) => {
                if (err) {
                    return reject(err)
                }
                _data = data
                resolve(data)
            })
        })

        // 重新获取接口配置数据
        await dispatch('getProjectApiConfig')

        return _data
    },
    // 获取项目的版本列表
    tagList({ commit, state }, payload) {
        return new Promise((resolve, reject) => {
            Ajax.tagList(state.projectId, (err, data) => {
                if (err) {
                    return reject(err)
                }
                resolve(data)
                commit('GET_PROJECT_VERSIONLIST', {
                    data: data
                })
            })
        })
    },
    // 新增项目的版本
    async addVersion({ commit, dispatch, state }, payload) {
        let _data = null

        await new Promise((resolve, reject) => {
            Ajax.addVersion(state.projectId, state.projectApiData.moduleList, (err, data) => {
                if (err) {
                    return reject(err)
                }
                _data = data
                resolve(data)
            })
        })

        // 重新获取版本列表
        await dispatch('tagList')

        // 更新本地当前版本
        const version = _data.version
        commit('SET_PROJECT_VERSION', {
            version: version
        })

        return _data
    }
}