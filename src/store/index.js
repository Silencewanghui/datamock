import Vue from 'vue';
import Vuex from 'vuex';
import actions from './actions'
import mutations from './mutations'
import APIURL from './APIURL'

Vue.use(Vuex)

const state = {
    APIURL: APIURL,
    loadingState: false, // 加载动画显示状态
    projectId: '', // 项目id
    projectName: '', // 项目名称
    projectIdentifier: '', // 项目标志
    version: 'v1', // 项目当前版本
    versionList: [], // 项目的版本列表
    projectApiData: {}
}

export default new Vuex.Store({
    state,
    actions,
    mutations
});