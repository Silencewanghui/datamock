import axios from 'axios';
import store from './store'

// 设置超时时间为十秒
axios.defaults.timeout = 10000

// 设置基本路由
// const BaseURL = 'http://192.168.20.32:3004/'

const BaseURL = 'http://127.0.0.1:3004/'

axios.defaults.baseURL = BaseURL + 'rest/'



const projectAxios = axios.create({
    baseURL: 'http://192.168.20.32:80/'
})

export default {
    /**
     * 获取所有的项目类型
     * @param {Function} callback
     */
    getAllProjectTypes: function(callback) {
        projectAxios.get('rest/type/types').then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络!', null);
        });
    },
    /**
     * 根据项目类型获取项目列表
     * @param {String} type
     * @param {Function} callback
     */
    getProjectWithType: function(type, callback) {
        projectAxios.get('rest/type/project', {
            params: {
                type: type
            }
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络!', null);
        });
    },
    /**
     * 根据项目id获取项目详情
     * @param {String} projectId
     * @param {Function} callback
     */
    getDetailWithId: function(projectId, callback) {
        projectAxios.get('rest/project/project', {
            params: {
                projectID: projectId
            }
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络!', null);
        });
    },
    /**
     * 新建项目接口配置数据
     * @param {String} projectId
     * @param {String} identifier
     * @param {Array} moduleList
     */
    addProject: function({ projectId, identifier, moduleList }, callback) {
        axios.post(store.state.APIURL.addProject, {
            projectId: projectId,
            identifier: identifier,
            moduleList: moduleList
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络！', null);
        });
    },
    /**
     * 开启某个项目的mock服务
     * @param {String} projectId
     * @param {String} identifier
     * @param {String} version
     * @param {Function} callback
     */
    startAPI: function({ projectId, identifier, version }, callback) {
        axios.post(store.state.APIURL.startAPI, {
            projectId: projectId,
            identifier: identifier,
            version: version
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.message);
        }).catch((err) => {
            callback('获取数据失败，请检查网络!', null);
        });
    },
    /**
     * 获取某个项目的接口配置数据
     * @param {String} projectId
     * @param {String} version
     * @param {Function} callback
     */
    api: function(projectId, version, callback) {
        axios.get(store.state.APIURL.api, {
            params: {
                projectId: projectId,
                version: version
            }
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络!', null);
        });
    },
    /**
     * 修改某个接口的配置数据
     * @param {String} projectId
     * @param {String} version
     * @param {Object} originIdentifier
     * @param {Object} updatedData
     */
    updateApi: function({
        projectId,
        version,
        originIdentifier,
        updatedData
    }, callback) {
        axios.post(store.state.APIURL.updateApi, {
            projectId,
            version,
            originIdentifier,
            updatedData
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络！', null);
        });
    },
    /**
     * 修改模块信息
     * @param {String} projectId
     * @param {String} version
     * @param {Object} originData
     * @param {Object} updatedData
     */
    updateModule: function(projectId, version, originData, updatedData, callback) {
        axios.post(store.state.APIURL.updateModule, {
            projectId: projectId,
            version: version,
            originData: originData,
            updatedData: updatedData
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络！', null);
        });
    },
    /**
     * mock服务预览
     * @param {String} URL
     * @param {Object} requestType
     * @param {Object} requestParam
     */
    mockPreview: function(URL, requestType, requestParam, callback) {
        // 创建自定义config的axios实例
        const mockAxios = axios.create({
            baseURL: BaseURL
        })

        requestType = requestType.toLowerCase()

        if (requestType === 'get' || requestType === 'delete') {
            mockAxios[requestType](URL, {
                params: requestParam
            }).then((res) => {
                if (res.data.status === -1) {
                    return callback(res.data.statusInfo.message, null);
                }
                callback(null, res.data.data);
            }).catch((err) => {
                callback('获取数据失败，请检查网络!', null);
            });
        } else if (requestType === 'post' || requestType === 'put') {
            mockAxios[requestType](URL, requestParam).then((res) => {
                if (res.data.status === -1) {
                    return callback(res.data.statusInfo.message, null);
                }
                callback(null, res.data.data);
            }).catch((err) => {
                callback('获取数据失败，请检查网络!', null);
            });
        }
    },
    /**
     * 添加接口
     * @param {String} projectId
     * @param {String} version
     * @param {Object} moduleData
     * @param {Object} apiData
     */
    addApi: function(projectId, version, moduleData, apiData, callback) {
        axios.post(store.state.APIURL.addApi, {
            projectId: projectId,
            version: version,
            moduleData: moduleData,
            apiData: apiData
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络！', null);
        });
    },
    /**
     * 添加模块
     * @param {String} projectId
     * @param {String} version
     * @param {Object} moduleData
     */
    addModule: function(projectId, version, moduleData, callback) {
        axios.post(store.state.APIURL.addModule, {
            projectId: projectId,
            version: version,
            moduleData: moduleData
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络！', null);
        });
    },
    /**
     * 删除模块
     * @param {String} projectId
     * @param {String} version
     * @param {Object} moduleData
     */
    delModule: function(projectId, version, moduleData, callback) {
        axios.post(store.state.APIURL.delModule, {
            projectId: projectId,
            version: version,
            moduleData: moduleData
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络！', null);
        });
    },
    /**
     * 删除接口
     * @param {String} projectId
     * @param {String} version
     * @param {Object} moduleData
     * @param {Object} apiData
     */
    delApi: function(projectId, version, moduleData, apiData, callback) {
        axios.post(store.state.APIURL.delApi, {
            projectId: projectId,
            version: version,
            moduleData: moduleData,
            apiData: apiData
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络！', null);
        });
    },
    /**
     * 获取项目的版本列表
     * @param {String} projectId
     */
    tagList: function(projectId, callback) {
        axios.get(store.state.APIURL.tagList, {
            params: {
                projectId: projectId
            }
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络！', null);
        });
    },
    /**
     * 新增项目的一个版本
     * @param {String} projectId
     * @param {Array} moduleList
     */
    addVersion: function(projectId, moduleList, callback) {
        axios.post(store.state.APIURL.addVersion, {
            projectId: projectId,
            moduleList: moduleList
        }).then((res) => {
            if (res.data.status === -1) {
                return callback(res.data.statusInfo.message, null);
            }
            callback(null, res.data.data);
        }).catch((err) => {
            callback('获取数据失败，请检查网络！', null);
        });
    }
}