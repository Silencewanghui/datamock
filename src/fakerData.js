﻿const fakerData = [{
    "type": "address",
    "typeName": "地址",
    "data": {
        "city": { "methodName": "city", "zh_CHS": "城市", "mockRule": "address&city" },
        "zipCode": { "methodName": "zipCode", "zh_CHS": "邮编", "mockRule": "address&zipCode" },
        "country": { "methodName": "country", "zh_CHS": "国家", "mockRule": "address&country" },
        "state": { "methodName": "state", "zh_CHS": "状态", "mockRule": "address&state" },
        "streetName": { "methodName": "streetName", "zh_CHS": "街道名称", "mockRule": "address&streetName" }
    }
}, {
    "type": "date",
    "typeName": "日期",
    "data": {
        "month": { "methodName": "month", "zh_CHS": "月", "mockRule": "date&month" },
        "weekday": { "methodName": "weekday", "zh_CHS": "工作日", "mockRule": "date&weekday" },
        "recent": { "methodName": "recent", "zh_CHS": "最近的", "mockRule": "date&recent" },
        "past": { "methodName": "past", "zh_CHS": "过去的", "mockRule": "date&past" },
        "future": { "methodName": "future", "zh_CHS": "未来", "mockRule": "date&future" }
    }    
}, {
    "type": "image",
    "typeName": "图片",
    "data": {
        "business": { "methodName": "business", "zh_CHS": "商业", "mockRule": "image&business" },
        "cats": { "methodName": "cats", "zh_CHS": "猫", "mockRule": "image&cats" },
        "animals": { "methodName": "animals", "zh_CHS": "动物", "mockRule": "image&animals" },
        "nature": { "methodName": "nature", "zh_CHS": "自然", "mockRule": "image&nature" },
        "abstract": { "methodName": "abstract", "zh_CHS": "抽象的", "mockRule": "image&abstract" },
        "food": { "methodName": "food", "zh_CHS": "食物", "mockRule": "image&food" },
        "avatar": { "methodName": "avatar", "zh_CHS": "头像", "mockRule": "image&avatar" },
        "city": { "methodName": "city", "zh_CHS": "城市", "mockRule": "image&city" },
        "nightlife": { "methodName": "nightlife", "zh_CHS": "夜生活", "mockRule": "image&nightlife" },
        "technics": { "methodName": "technics", "zh_CHS": "工艺", "mockRule": "image&technics" },
        "image": { "methodName": "image", "zh_CHS": "影像", "mockRule": "image&image" },
        "fashion": { "methodName": "fashion", "zh_CHS": "时尚", "mockRule": "image&fashion" },
        "sports": { "methodName": "sports", "zh_CHS": "运动的", "mockRule": "image&sports" },
        "transport": { "methodName": "transport", "zh_CHS": "运输", "mockRule": "image&transport" },
        "people": { "methodName": "people", "zh_CHS": "人类", "mockRule": "image&people" }
    }    
}, {
    "type": "commerce",
    "typeName": "商业",
    "data": {
        "color": { "methodName": "color", "zh_CHS": "颜色", "mockRule": "commerce&color" },
        "productName": { "methodName": "productName", "zh_CHS": "产品名称", "mockRule": "commerce&productName" },
        "productMaterial": { "methodName": "productMaterial", "zh_CHS": "产品材料", "mockRule": "commerce&productMaterial" },
        "price": { "methodName": "price", "zh_CHS": "价格", "mockRule": "commerce&price" },
        "department": { "methodName": "department", "zh_CHS": "部门", "mockRule": "commerce&department" },
        "product": { "methodName": "product", "zh_CHS": "产品", "mockRule": "commerce&product" }
    }    
}, {
    "type": "internet",
    "typeName": "网络",
    "data": {
        "protocol": { "methodName": "protocol", "zh_CHS": "协议", "mockRule": "internet&protocol" },
        "mac": { "methodName": "mac", "zh_CHS": "mac地址", "mockRule": "internet&mac" },
        "color": { "methodName": "color", "zh_CHS": "颜色", "mockRule": "internet&color" },
        "domainSuffix": { "methodName": "domainSuffix", "zh_CHS": "域名后缀", "mockRule": "internet&domainSuffix" },
        "domainName": { "methodName": "domainName", "zh_CHS": "域名", "mockRule": "internet&domainName" },
        "ipv6": { "methodName": "ipv6", "zh_CHS": "IPv6地址", "mockRule": "internet&ipv6" },
        "ip": { "methodName": "ip", "zh_CHS": "ip地址", "mockRule": "internet&ip" },
        "url": { "methodName": "url", "zh_CHS": "url地址", "mockRule": "internet&url" },
        "userName": { "methodName": "userName", "zh_CHS": "用户名", "mockRule": "internet&userName" },
        "email": { "methodName": "email", "zh_CHS": "电子邮件", "mockRule": "internet&email" },
        "avatar": { "methodName": "avatar", "zh_CHS": "头像", "mockRule": "internet&avatar" },
        "password": { "methodName": "password", "zh_CHS": "密码", "mockRule": "internet&password" },
        "userAgent": { "methodName": "userAgent", "zh_CHS": "用户代理", "mockRule": "internet&userAgent" }
    }    
}, {
    "type": "phone",
    "typeName": "手机",
    "data": {
        "phoneNumber": { "methodName": "phoneNumber", "zh_CHS": "手机号", "mockRule": "phone&phoneNumber" }
    }    
}, {
    "type": "lorem",
    "typeName": "文章",
    "data": {
        "lines": { "methodName": "lines", "zh_CHS": "线条", "mockRule": "lorem&lines" },
        "paragraph": { "methodName": "paragraph", "zh_CHS": "段", "mockRule": "lorem&graph" },
        "text": { "methodName": "text", "zh_CHS": "正文", "mockRule": "lorem&text" },
        "paragraphs": { "methodName": "paragraphs", "zh_CHS": "段落", "mockRule": "lorem&graphs" },
        "word": { "methodName": "word", "zh_CHS": "词", "mockRule": "lorem&word" },
        "words": { "methodName": "words", "zh_CHS": "言语", "mockRule": "lorem&words" },
        "sentence": { "methodName": "sentence", "zh_CHS": "句子", "mockRule": "lorem&sentence" }
    }    
}, {
    "type": "random",
    "typeName": "随机数据",
    "data": {
        "number": { "methodName": "number", "zh_CHS": "编号", "mockRule": "random&number" },
        "word": { "methodName": "word", "zh_CHS": "词", "mockRule": "random&word" },
        "image": { "methodName": "image", "zh_CHS": "影像", "mockRule": "random&image" },
        "words": { "methodName": "words", "zh_CHS": "言语", "mockRule": "random&words" },
        "alphaNumeric": { "methodName": "alphaNumeric", "zh_CHS": "字母数字", "mockRule": "random&alphaNumeric" },
        "boolean": { "methodName": "boolean", "zh_CHS": "布尔值", "mockRule": "random&boolean" }
    }    
}, {
    "type": "name",
    "typeName": "姓名",
    "data": {
        "findName": { "methodName": "findName", "zh_CHS": "姓名", "mockRule": "name&findName" },
        "jobType": { "methodName": "jobType", "zh_CHS": "职位类型", "mockRule": "name&jobType" },
        "title": { "methodName": "title", "zh_CHS": "职位", "mockRule": "name&title" }
    }    
}]

module.exports = fakerData