import Vue from 'vue';
import Router from 'vue-router';
import store from '../store';

Vue.use(Router);

const router = new Router({
    routes: [{
        path: '/',
        redirect: '/selectProject'
    }, {
        path: '/selectProject',
        component: resolve => require(['../components/page/ProjectList.vue'], resolve)
    }, {
        path: '/home',
        component: resolve => require(['../components/common/Home.vue'], resolve),
        children: [{
            path: '/',
            redirect: '/home/apiConfig'
        }, {
            path: 'apiConfig',
            component: resolve => require(['../components/page/ApiConfig/ApiConfig.vue'], resolve)
        }]
    }]
})

router.beforeEach((to, from, next) => {
    if (to.fullPath === '/home/apiConfig' && !store.state.projectId &&
        !store.state.projectName) {
        return next('/selectProject')
    }
    next()
})

export default router