const HandleParameter = require('../../../../server/routes/api/handleAPIConfig').handleParameter
const GenerateMockData = require('../../../../server/routes/api/generateMockData').generateMockData

function generateRequestParams(paramRules) {
    try {
        const rules = HandleParameter(paramRules)
        const mockData = GenerateMockData(rules)
        return mockData
    } catch (e) {
        return e
    }
}

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function(match) {
        var cls = 'code_style_number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'code_style_key';
            } else {
                cls = 'code_style_string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'code_style_boolean';
        } else if (/null/.test(match)) {
            cls = 'code_style_null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

module.exports = {
    generateRequestParams: generateRequestParams,
    syntaxHighlight: syntaxHighlight
}