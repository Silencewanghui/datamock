import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store'
import axios from 'axios';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'
import "babel-polyfill";

Vue.use(ElementUI);

Vue.prototype.$axios = axios;

var EventBus = new Vue();

Object.defineProperty(Vue.prototype, '$eventBus', {
    get: function() {
        return this.$root.eventBus
    }
})

new Vue({
    data() {
        return {
            eventBus: EventBus
        }
    },
    store,
    router,
    render: h => h(App)
}).$mount('#app');