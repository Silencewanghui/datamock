FROM node:8.2.1-slim
MAINTAINER bsxian

RUN useradd --user-group --create-home --shell /bin/false app

EXPOSE 3004
ENV NODE_ENV=production
ENV HOME=/home/app

USER root

ADD ./server $HOME/server

# WORKDIR /app/server

# RUN  npm start
# COPY config.js package.json $HOME/server
# ADD  ./ $HOME/server

WORKDIR $HOME/server
# RUN npm install
RUN chown -R app:app $HOME/*

USER app

CMD ["npm", "run", "start"]


